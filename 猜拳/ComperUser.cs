﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 猜拳
{
    class ComperUser
    {
        public string FistName
        {
            get;
            set;
        }

        public int ShowFist()
        {
            Random random = new Random();
            int r = random.Next(1, 4);

            switch (r)
            {
                case 1:
                    this.FistName = "石头";
                    break;
                case 2:
                    this.FistName = "剪刀";
                    break;
                case 3:
                    this.FistName = "布";
                    break;
            }

            return r;
        }
    }
}
