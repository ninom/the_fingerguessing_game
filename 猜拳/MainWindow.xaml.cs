﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace 猜拳
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if(btn != null)
            {
                UserPlayer ul = new UserPlayer();
                int userFist = ul.ShowFist(btn.Content.ToString());
                labWan.Content = ul.FistName;

                ComperUser pc1 = new ComperUser();
                int computerFist = pc1.ShowFist();
                labCom.Content = pc1.FistName;

                CaiPan cp = new CaiPan();
                labResult.Content = cp.IsUserWin(userFist, computerFist);
            }

        }


    }
}
